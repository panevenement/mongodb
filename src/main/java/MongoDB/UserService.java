package MongoDB;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    MongoClient mongoClient;
    MongoDatabase mongoDatabase;
    MongoCollection<Document> collection;

    public UserService(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
        this.mongoDatabase = mongoClient.getDatabase("userDataBase");
        this.collection = mongoDatabase.getCollection("userCollection");
    }

    public void saveUser(User user) {
        collection.insertOne(UserParser.getDocument(user));
    }

    public List<User> getAllUsers() {
        List<User> newList = new ArrayList<>();
        for (Document cur : collection.find()) {
            User user = UserParser.getUser(cur);
            newList.add(user);
        }

        return newList;
    }

    public User getUserByPermission(String doc) {
        List<User> permissionList = new ArrayList<>();
        User user;
        for (Document cur : collection.find(Filters.eq("permission", doc))) {
            user = UserParser.getUser(cur);
            permissionList.add(user);
        }
        return (User) permissionList;
    }
}
