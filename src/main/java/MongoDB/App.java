package MongoDB;


import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        MongoClient mongoClient = MongoClients.create();

        User user1 = new User(1,"jagoda","jagoda1","Adrian","Derda");
        User user2 = new User(2,"Szade","Szade1","Michał","Michalski");
        User user3 = new User(3,"Byku","Byku1","Natalia","Iwaszkiewicz");
        User user4 = new User(4,"Fiku","Fiku1","Patrycja","Poeta");

        UserService service = new UserService(mongoClient);
        service.saveUser(user1);
        service.saveUser(user2);
        service.saveUser(user3);
        service.saveUser(user4);

        user1.setPermission(new Permission(1,"admin"));


        service.getUserByPermission(new Document("1","admin"));





    }
}
