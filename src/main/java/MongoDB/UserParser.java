package MongoDB;


import org.bson.Document;

public class UserParser {


    public static Document getDocument(User user){

       Document doc = new Document("id", user.getId())
                .append("login", user.getLogin())
                .append("password",user.getPassword())
                .append("name",user.getName())
                .append("lastName",user.getLastName())
               .append("permission", user.getPermission());

        return doc;
    }

    public static User getUser(Document doc){
        User user = new User();
        user.setId(doc.getInteger("id"));
        user.setLogin(doc.getString("login"));
        user.setPassword(doc.getString("password"));
        user.setName(doc.getString("name"));
        user.setLastName(doc.getString("lastName"));
        user.setPermission(UserParser.getUser(doc.getString("permission")));
        return user;
    }
}
